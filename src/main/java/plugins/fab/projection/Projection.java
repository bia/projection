package plugins.fab.projection;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import icy.gui.dialog.MessageDialog;
import icy.gui.frame.IcyFrame;
import icy.gui.util.GuiUtil;
import icy.image.IcyBufferedImage;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginImageAnalysis;
import icy.sequence.Sequence;
import icy.system.thread.ThreadUtil;
import icy.type.collection.array.Array1DUtil;

/**
 * @author Fabrice de Chaumont
 *         Performs projection over Z or T, with min,max,mean,sum, sum unsaturated criteria.
 */
public class Projection extends Plugin implements PluginImageAnalysis, ActionListener
{

    JButton startButton = new JButton("Start");

    String[] projectionAxis = {"Z Projection", "T Projection"};

    JComboBox comboProjectionAxis = new JComboBox(projectionAxis);

    String[] projectionType = {"Maximum", "Mean", "Minimum", "Sum (saturated)", "Sum (non-saturated)",};

    JComboBox comboProjectionType = new JComboBox(projectionType);

    @Override
    public void compute()
    {

        JPanel panel = new JPanel();
        IcyFrame frame = GuiUtil.generateTitleFrame("Projection", panel, new Dimension(200, 100), true, true, true,
                true);

        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));

        panel.setBorder(new TitledBorder("Projection setup"));
        panel.add(GuiUtil.createLineBoxPanel(Box.createVerticalStrut(10)));
        panel.add(GuiUtil.createLineBoxPanel(new JLabel("Projection axis:")));
        panel.add(GuiUtil.createLineBoxPanel(Box.createVerticalStrut(10)));
        panel.add(GuiUtil.createLineBoxPanel(Box.createHorizontalStrut(20), comboProjectionAxis,
                Box.createHorizontalStrut(20)));

        panel.add(GuiUtil.createLineBoxPanel(Box.createVerticalStrut(20)));
        panel.add(GuiUtil.createLineBoxPanel(new JLabel("Projection type:")));
        panel.add(GuiUtil.createLineBoxPanel(Box.createVerticalStrut(10)));
        panel.add(GuiUtil.createLineBoxPanel(Box.createHorizontalStrut(20), comboProjectionType,
                Box.createHorizontalStrut(20)));

        panel.add(GuiUtil.createLineBoxPanel(Box.createVerticalStrut(20)));
        startButton.addActionListener(this);
        panel.add(
                GuiUtil.createLineBoxPanel(Box.createHorizontalStrut(20), startButton, Box.createHorizontalStrut(20)));
        panel.add(GuiUtil.createLineBoxPanel(Box.createVerticalStrut(20)));

        frame.pack();
        frame.addToDesktopPane();
        frame.center();
        frame.setVisible(true);
        frame.requestFocus();

    }

    @Override
    public void actionPerformed(ActionEvent e)
    {

        if (e.getSource() == startButton)
        {
            final Sequence sequence = getActiveSequence();
            if (sequence == null)
            {
                MessageDialog.showDialog("Please open a sequence to use this plugin.", MessageDialog.WARNING_MESSAGE);
                return;
            }

            final ProjectionAxis projAxis;

            switch (comboProjectionAxis.getSelectedIndex())
            {
                default:
                case 0:
                    projAxis = ProjectionAxis.Z_PROJECTION;
                    break;
                case 1:
                    projAxis = ProjectionAxis.T_PROJECTION;
                    break;
            }

            final ProjectionType projType;

            switch (comboProjectionType.getSelectedIndex())
            {
                default:
                case 0:
                    projType = ProjectionType.MAXIMUM;
                    break;
                case 1:
                    projType = ProjectionType.MEAN;
                    break;
                case 2:
                    projType = ProjectionType.MINIMUM;
                    break;
                case 3:
                    projType = ProjectionType.SUM_SATURATED;
                    break;
            }

            startButton.setEnabled(false);
            ThreadUtil.bgRun(new Runnable()
            {
                @Override
                public void run()
                {
                    try
                    {
                        Project(sequence, projAxis, projType);
                    }
                    finally
                    {
                        ThreadUtil.invokeLater(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                startButton.setEnabled(true);
                            }
                        });
                    }
                }
            });
        }
    }

    public enum ProjectionAxis
    {
        Z_PROJECTION, T_PROJECTION;
    }

    public enum ProjectionType
    {
        MAXIMUM, MINIMUM, MEAN, SUM_SATURATED, SUM_NOT_SATURATED;
    }

    public static void Project(Sequence sequence, ProjectionAxis axis, ProjectionType type)
    {
        if (axis == ProjectionAxis.Z_PROJECTION)
        {
            sequence.beginUpdate();
            try
            {
                for (int t = 0; t < sequence.getSizeT(); t++)
                {
                    ArrayList<IcyBufferedImage> images = sequence.getImages(t);
                    if (images == null)
                    {
                        MessageDialog.showDialog("Projection cannot be performed: there is no image at time t=" + t,
                                MessageDialog.ERROR_MESSAGE);
                        break;
                    }
                    if (images.size() == 0)
                        continue;

                    IcyBufferedImage projectedImage = null;

                    if (type == ProjectionType.SUM_SATURATED)
                    {
                        projectedImage = add(images, true);
                    }
                    if (type == ProjectionType.SUM_NOT_SATURATED)
                    {
                        projectedImage = add(images, false);
                    }
                    if (type == ProjectionType.MAXIMUM)
                    {
                        projectedImage = max(images);
                    }
                    if (type == ProjectionType.MINIMUM)
                    {
                        projectedImage = min(images);
                    }
                    if (type == ProjectionType.MEAN)
                    {
                        projectedImage = mean(images);
                    }

                    sequence.removeAllImages(t);
                    sequence.setImage(t, 0, projectedImage);
                }
            }
            finally
            {
                sequence.endUpdate();
            }
        }

        if (axis == ProjectionAxis.T_PROJECTION)
        {
            sequence.beginUpdate();
            try
            {
                mainLoop: for (int z = 0; z < sequence.getSizeZ(); z++)
                {
                    ArrayList<IcyBufferedImage> images = new ArrayList<IcyBufferedImage>();
                    for (int t = 0; t < sequence.getSizeT(); t++)
                    {
                        IcyBufferedImage imageToAdd = sequence.getImage(t, z);
                        if (imageToAdd == null)
                        {
                            MessageDialog.showDialog(
                                    "Projection cannot be performed: there is no image at t=" + t + " z=" + z,
                                    MessageDialog.ERROR_MESSAGE);
                            break mainLoop;
                        }

                        images.add(sequence.getImage(t, z));
                    }

                    if (images.size() == 0)
                        continue;

                    IcyBufferedImage projectedImage = null;

                    if (type == ProjectionType.SUM_SATURATED)
                    {
                        projectedImage = add(images, true);
                    }
                    if (type == ProjectionType.SUM_NOT_SATURATED)
                    {
                        projectedImage = add(images, false);
                    }
                    if (type == ProjectionType.MINIMUM)
                    {
                        projectedImage = min(images);
                    }
                    if (type == ProjectionType.MEAN)
                    {
                        projectedImage = mean(images);
                    }
                    if (type == ProjectionType.MAXIMUM)
                    {
                        projectedImage = max(images);
                    }

                    for (int t = 0; t < sequence.getSizeT(); t++)
                    {
                        sequence.removeImage(t, z);
                    }
                    sequence.setImage(0, z, projectedImage);

                }
            }
            finally
            {
                sequence.endUpdate();
            }
        }
        sequence.dataChanged();

    }

    public static IcyBufferedImage add(ArrayList<IcyBufferedImage> images, boolean check_for_saturated)
    {
        IcyBufferedImage finalImage = images.get(0).getCopy();

        for (int channel = 0; channel < images.get(0).getSizeC(); channel++)
        {
            double[] resultDataBuffer = new double[images.get(0).getWidth() * images.get(0).getHeight()];

            for (IcyBufferedImage image : images)
            {
                double[] dataBuffer = Array1DUtil.arrayToDoubleArray(image.getDataXY(channel),
                        image.isSignedDataType());
                for (int i = 0; i < dataBuffer.length; i++)
                {
                    resultDataBuffer[i] += dataBuffer[i];
                }
            }

            if (check_for_saturated)
            {
                double[] bounds = finalImage.getDataType_().getDefaultBounds();
                double max = bounds[1];
                for (int i = 0; i < resultDataBuffer.length; i++)
                {
                    if (resultDataBuffer[i] > max)
                        resultDataBuffer[i] = max;
                }
            }

            final Object dest = finalImage.getDataXY(channel);
            Array1DUtil.doubleArrayToArray(resultDataBuffer, dest);
            // for data changed and proper cache update
            finalImage.setDataXY(channel, dest);
        }

        return finalImage;

    }

    public static IcyBufferedImage mean(ArrayList<IcyBufferedImage> images)
    {
        IcyBufferedImage finalImage = images.get(0).getCopy();

        for (int channel = 0; channel < images.get(0).getSizeC(); channel++)
        {
            double[] resultDataBuffer = new double[images.get(0).getWidth() * images.get(0).getHeight()];

            for (IcyBufferedImage image : images)
            {
                double[] dataBuffer = Array1DUtil.arrayToDoubleArray(image.getDataXY(channel),
                        image.isSignedDataType());
                for (int i = 0; i < dataBuffer.length; i++)
                {
                    resultDataBuffer[i] += dataBuffer[i];
                }
            }

            for (int i = 0; i < resultDataBuffer.length; i++)
            {
                resultDataBuffer[i] /= images.size();
            }

            final Object dest = finalImage.getDataXY(channel);
            Array1DUtil.doubleArrayToArray(resultDataBuffer, dest);
            // for data changed and proper cache update
            finalImage.setDataXY(channel, dest);
        }

        return finalImage;

    }

    public static IcyBufferedImage max(ArrayList<IcyBufferedImage> images)
    {
        IcyBufferedImage finalImage = images.get(0).getCopy();

        for (int channel = 0; channel < images.get(0).getSizeC(); channel++)
        {
            double[] resultDataBuffer = Array1DUtil.arrayToDoubleArray(images.get(0).getDataXY(channel),
                    images.get(0).isSignedDataType());

            for (IcyBufferedImage image : images)
            {
                double[] dataBuffer = Array1DUtil.arrayToDoubleArray(image.getDataXY(channel),
                        image.isSignedDataType());
                for (int i = 0; i < dataBuffer.length; i++)
                {
                    if (resultDataBuffer[i] < dataBuffer[i])
                        resultDataBuffer[i] = dataBuffer[i];
                }
            }

            final Object dest = finalImage.getDataXY(channel);
            Array1DUtil.doubleArrayToArray(resultDataBuffer, dest);
            // for data changed and proper cache update
            finalImage.setDataXY(channel, dest);
        }

        return finalImage;
    }

    public static IcyBufferedImage min(ArrayList<IcyBufferedImage> images)
    {
        IcyBufferedImage finalImage = images.get(0).getCopy();

        for (int channel = 0; channel < images.get(0).getSizeC(); channel++)
        {
            double[] resultDataBuffer = Array1DUtil.arrayToDoubleArray(images.get(0).getDataXY(channel),
                    images.get(0).isSignedDataType());

            for (IcyBufferedImage image : images)
            {
                double[] dataBuffer = Array1DUtil.arrayToDoubleArray(image.getDataXY(channel),
                        image.isSignedDataType());
                for (int i = 0; i < dataBuffer.length; i++)
                {
                    if (resultDataBuffer[i] > dataBuffer[i])
                        resultDataBuffer[i] = dataBuffer[i];
                }
            }

            final Object dest = finalImage.getDataXY(channel);
            Array1DUtil.doubleArrayToArray(resultDataBuffer, dest);
            // for data changed and proper cache update
            finalImage.setDataXY(channel, dest);
        }

        return finalImage;
    }
}
